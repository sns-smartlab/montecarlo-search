#!/usr/bin/env python	
" A parser: 1) Do dihedral Montecarlo move, 2) Do single point with Gaussian, 3) Accept/reject Moves"

import  numpy as np      ;    import  math 
import argparse as argp  ;    import os.path      
from   sys  import exit  ;    import time              
import subprocess as sub ;    import pandas as pd  
from MCARLO_SUB    import zetamat ;    import random

Parse = argp.ArgumentParser()
Parse.add_argument('-I'  , '--INPS'  ,  help='Input structure as zeta matrix'   ,  default='test.com'  , type=str)
Parse.add_argument('-N'  , '--NMOVE' ,  help='Numer of random moves needed'     ,  default= 5       , type=int)
Parse.add_argument('-T'  , '--TEMP'  ,  help='Temperature, default 1K        '  ,  default=298.5         , type=float)
Parse.add_argument('-P'  , '--DPAR'  ,  help='Dihderal bond atoms as tuple   '  ,  default=[(20,7),(21,20),(50,8),(53,50),(55,53), (61,55) ] , type=list)
Parse.add_argument('-O'  , '--OUTF'  ,  help='Outpuf file to store data      '  ,  default='Zematv1.dat' , type=str  )
Parse.add_argument('-E'  , '--EREF'  ,  help='Starting Energy (Hartree) '       ,  default= -119.0101    , type=float)
Parse.add_argument('-OP' , '--PREF'  ,  help='Prefix for output files  '        ,  default= 'RUN'        , type=str)
Parse.add_argument('-ND' , '--NDIH'  ,  help='Number of Dihedrals to change'    ,  default= 2            , type=int) 
Parse.add_argument('-MT' , '--MTOD'  ,  help='Level of theory for Guassian'     ,  default= 'am1' , type=str)
Myarg = Parse.parse_args()

sub.call('clear',shell=True)

   
if not os.path.isfile(Myarg.INPS):
    print("*********************************")
    print("Input Gaussian file doesnt exist" )
    print("*********************************")
    exit(1)
   
# Set some inputs

beta  =  1/( 3.166625e-6 * Myarg.TEMP )  # K in Hartree, T as 1K, precalculate beta value 
Eref  =  Myarg.EREF                      #Energy for starting structure,try a SP calcuations
 
# No. of dihedrals
nDih  = len(Myarg.DPAR)                  # List of atom-pairs involving the torsion to be changed
nD    =     Myarg.NDIH                   # Number of torsions to be used for random moves

# Input zmat config file
sub.call('cp %s temp.com' % Myarg.INPS, shell=True)   # Copy the user zmat to a temprovary file
Pconfig = 'temp.com'                                 # Read the user zmat  
status = 0                                            # Status initialization  
natm = 10                                             # Number of atoms in the system, used only when optizatn is done during MC
# Remove the existing log files
sub.call('rm    Accept.log',shell=True)               # Remove log files if already existing
sub.call('touch Accept.log',shell=True)

# Array to store all data
Ensem = np.zeros( (Myarg.NMOVE, nDih+6 ))            # Data Array Format: Angle1 Angle2 ... Anglen  Eref  Ecur Pval Bw Status Eopt 

discrt = np.arange( -180 , 190.0 , 30 )
NR_arr = {}

# Just create one random element in the dictionary
NR_arr[-1] = np.array( [1000]*(nDih+nD) )

def dmove(Gang,toss):
    # Create a zero array with n-diherals based on number of atom-pairs in Myarg.DPAR
    cmov  = np.zeros( (nDih) )
    # toss to see nD (say 2) out of nDih (say 6) to be moved, populate Gangles into  cmov
    random.seed()
    DRest = []

    if nD < nDih:
        # Dnt toss here as in old code, pass it from outside
        #toss =  random.sample( range(0,nDih) , nD )
        cmov[ toss ] = Gang   
        # Use ZMAT class to do angle increments/random changes, save output as zmatrix and gaussin input .com
        for indx in toss:
            zm.dih_rand( Myarg.DPAR[indx] , cmov[indx] )
            DRest.append("*  %2d  %2d *  F\n" % (Myarg.DPAR[indx][0] , Myarg.DPAR[indx][1] ) )
    
    else:
        cmov = Gang   
        # Use ZMAT class to do angle increments/random changes, save output as zmatrix and gaussin input .com
        for indx in range(nDih):
            zm.dih_rand( Myarg.DPAR[indx] , cmov[indx] )
            DRest.append("*  %2d  %2d *  F\n" % (Myarg.DPAR[indx][0] , Myarg.DPAR[indx][1] ) )

    # Here indicate optimization, dihedral constraint and multile-level jobs (i.e, low level opt + Single point)
    zm.write_zmat('cmove.mat')
    zm.write_com('cmove.com',OPT=True,REST=DRest,MJOB=True)
    return cmov       

def chk_redun(arr):
    Full = np.array( list(NR_arr.values()) ) 
    X =  np.abs( arr - Full )
    return (X<2.5).all(axis=1)

def archeck(Ea,Eb):
    #Keep chaning the global variables
    global Eref,status
    
    np.random.seed()
    P = np.random.random_sample() 

    dE = ( Eb - Ea ) 

    if dE <= 0.0:
        status = 1
        Eref = Eb
        # For post-debugging, assing this number when boltzman criteria wasnt used
        P = 999 ; Wt = 999
    
    if dE > 0.0:
        Blz = dE * beta
        Wt  = np.exp(-Blz) 

        if  P < Wt:
            status = 1
            Eref = Eb

        else :
            status = 0

    return P,Wt,status

start = time.time()
nmove = 0 ; count = 0


while nmove < Myarg.NMOVE:

    # First read the zeta matrix
    zm = zetamat( Pconfig )
    Natm = zm.read_mat()
    # Generate n random angle values from discrete set  
    np.random.seed()
    rangles  = np.random.choice(discrt , size=nD)
    Gangles  = np.random.normal( rangles , scale=5.0 , size=nD )
    toss =  random.sample( range(0,nDih) , nD )
    carray = np.array( [1000]*(nDih+nD) )
    carray[toss] = 1
    carray[nDih:] = Gangles

    count += 1

    if True in chk_redun(carray):
        continue 

    else:
        NR_arr[nmove] = carray
        # Make the dihedral moves calling dmove,store increment values (for debug purpose)
        Ensem[nmove,:nDih] = dmove(Gangles,toss)
        
           
        # Call Gaussian SP on the current move and take the energy value
        sub.call('g16    <  cmove.com  >  cmove.log ',shell=True)

        # After rand. moves, if SP fails due to atom clash, set status to 2, continue next move 
        #output = sub.check_output("tail -1 cmove.log | awk '{print $1}' " , shell=True)
        output = sub.getoutput("tail -1 cmove.log | awk '{print $1}' ")
        #print(output) 
        if  not 'Normal' in output:
            status = 2
            Ensem[nmove,nDih+4] =  status
            print('Optimization failure at iterantion:%d' % nmove)
                          
        else:
            #******************************************** 
            # Extracting the energy from gauss log
            inp = [ lines for lines in open('cmove.log')  if "SCF Done:"  in lines ]
            #******************************************** 
            Ecur = float( inp[-1].split()[4] )
            # Populate Eref, Ecur, P, Bw, Status
            Ensem[nmove , nDih]   = Eref
            Ensem[nmove , nDih+1] = Ecur
            Ensem[nmove , nDih+2:nDih+5] =  archeck( Ea = Eref, Eb = Ecur)

            # When a move is accepted, a) Move current  matrix as tmp.com but always use the user zmat for the next itrn, b) update Accept.log

            if status == 1:
                zm.ext_zmat(Log='cmove.log')
                zm.write_zmat('temp.com')
                zm.ext_cart(Log='cmove.log')
                zm.write_cart('tmp.com')
                Pref = Myarg.PREF + '_%05d' % nmove
                sub.call("printf  ' %s\n'        >> Accept.log" % Natm , shell =True)
                sub.call("printf  '%s: %13.9f\n' >> Accept.log" % (Pref,Ecur) , shell =True)
                sub.call("cat  tmp.com           >> Accept.log" , shell =True)
                sub.call("printf  '\n'           >> Accept.log" , shell =True)
                #sub.call('cp cmove.log %s.log' % Pref,shell =True)
                Ensem[nmove , -1 ] = sub.getoutput("grep '  Tot=' cmove.log  | sed --expression='s/D/e/g' | tail -1 | awk '{print $8}' " )

            sub.call('rm *.chk',shell =True)
            #Dnt account for Gaus opt erros     
            nmove += 1

out_fl = open(Myarg.OUTF,'w')
a = [ '#Angle '.ljust(17) ] * nDih  
out_fl.write( ''.join(a) + '#Eref'.ljust(17) + '#Ecur'.ljust(17) + '#Pr'.ljust(17) + '#BW'.ljust(17)  +  '#Stat'.ljust(17) + '#DipoleM'.ljust(17) + '\n' )
np.savetxt( out_fl , Ensem , fmt='%13.9f', delimiter='\t')

# Close all file handles
out_fl.close() 
print('Total trials generated %d' % count ) 
print('TOTAL TIME in SECONDS FOR MONTECARLO\n', time.time() - start )
