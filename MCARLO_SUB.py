#!/usr/bin/env python   
#*************VERSION 22/01/2019*************
#********Created by C.Bala*******************

import numpy as np ; np.set_printoptions(suppress=True)
import pandas as pd
import subprocess as sub

#!/usr/bin/env python   


class zetamat:
    
    def __init__(self,name):
        #print "A class to handle zmatrix with pandas"
        self.zmat   = name
        self.heads = ['Element','Bwith','Dist','Awith','Angle','Dwith','Dihedral']
        self.chead = ['Element' , 'X' , 'Y' , 'Z']
        self.f = lambda D: D+360 if D<-180 else (D-360 if D>180 else D)
        self.forms= { 'Dist':'{:,.3f}'.format , 'Angle':'{:,.3f}'.format ,  'Dihedral':'{:,.3f}'.format }
        
    def read_mat(self):
        zmat = pd.read_table(self.zmat,delim_whitespace=True, comment='#',names=self.heads)
        natm = zmat.shape[0]
        zmat.index= range(1,natm+1)
        zmat['Bwith'] = zmat['Bwith'].fillna(-1).astype(int)
        zmat['Awith'] = zmat['Awith'].fillna(-1).astype(int)
        zmat['Dwith'] = zmat['Dwith'].fillna(-1).astype(int)
        rots = list( zip(zmat.Bwith, zmat.Awith) ) 
        zmat['Rot_pairs'] = rots

        # Additional columns are added to zmatrix  replace optimized parameters
        Blist  = [ 'R('+str(x)+','+str(y)+')' for x,y in zip(zmat.Bwith[1:],zmat.index[1:]) ]
        Alist  = [ 'A('+str(x)+','+str(y)+','+str(z)+')'   for x,y,z in zip(zmat.Awith[2:],zmat.Bwith[2:],zmat.index[2:]) ]
        Dlist  = [ 'D('+str(a)+','+str(b)+','+str(c)+','+str(d)+')' for a,b,c,d in zip(zmat.Dwith[3:],zmat.Awith[3:],zmat.Bwith[3:],zmat.index[3:]) ]

        Blist.insert(0,'-1')
        zmat['Batoms'] = Blist

        for i in [0,1]: Alist.insert(i,'-1')
        zmat['Aatoms'] = Alist

        for i in [0,1,2]: Dlist.insert(i,'-1')
        zmat['Datoms'] = Dlist 
                     
        self.zmat = zmat
        self.N = self.zmat.shape[0]

        return self.N
    def zprint(self):
        '''Simple print function to print the zmatrix'''
        print(self.zmat)
        
    def dih_incr(self,dpair,incr):
        '''Given a atompair involved in a torion, this function increments the current
           dihedral value by specified incr value''' 

        # incr = Increment to current dihedral
        # Idenfity index of pandas rows that have the atom pairs
        dih_ind = self.zmat[ self.zmat['Rot_pairs'] == dpair ].index - 1

        # New dih = (increment + original value, adjusted to be  -180 to 180)
        # Do increments to all the torsions involving the same atom pair            
        for i in dih_ind:
            self.zmat.iloc[i,6] = self.f( self.zmat.iloc[i,6] + incr )
        
        #print self.zmat[ self.zmat['Rot_pairs'] == dpair ]

    def dih_rand(self,dpair,drand):
        '''Given a atompair involved in a torion, this function changes the dihedral  
           to the newly specified random value''' 

        # drand = New random dihedral 
        # Idenfity index of pandas rows that have the atom pairs
        dih_ind = self.zmat[ self.zmat['Rot_pairs'] == dpair ].index - 1
        
        # Firstly, obtain the difference (i.e., new - old, adjust between -180 to 180 )
        dT = self.f (  drand - self.zmat.iloc[dih_ind[0],6]  )

        # For the first item, apply the drand directly
        # For other torsions involving the same atom pair, substitute the difference
        self.zmat.iloc[dih_ind[0],6] = drand
        
        for i in dih_ind[1:]:
            self.zmat.iloc[i,6] = self.f( self.zmat.iloc[i,6] + dT )
                  
    def write_com(self,NAME,OPT=False,NCPU=16,THR1='PM7 freq ',REST=None,MJOB=None,THR2='b3lyp/6-31+g(d) EmpiricalDispersion=GD3'): 
        self.out   = NAME
        self.opt   = OPT
        self.cpu   = NCPU
        self.thr1  = THR1
        self.thr2  = THR2
        self.rest  = REST
        self.mjob  = MJOB

        parms = [ '%mem=50GB\n','%%nprocshared=%d\n'%self.cpu,'%chk=current.chk\n',\
                 '#  %s  nosymm int=ultrafine  \n' % self.thr1 ]
        if self.opt == True:
            parms[-1] = '#  %s  nosymm int=ultrafine opt=(verytight,MaxCycles=100)\n' % self.thr1  
        if self.rest !=None:
            parms[-1] = '#  %s  nosymm int=ultrafine opt=(verytight,MaxCycles=100,ModRedundant)\n' % self.thr1  
            
        myout = open( self.out , 'w')
        myout.write(''.join(parms))
        myout.write('\nMYMOL\n\n0 1\n')
        myout.write( self.zmat.to_string(header=False,index=False,na_rep='',\
                    columns=self.heads,float_format='%13.10f') )
        myout.write('\n\n')

        if self.rest != None:
            myout.write(''.join(self.rest) )
            myout.write('\n')

        if self.thr1 in ['DFTBA']:
            myout.write('@dftba.prm\n\n')

        #myout.write('@minis.gbs\n\n')
        #myout.write('@minis.acp\n\n')

        if self.mjob != None:
            parms[-1] = '#  %s  nosymm int=ultrafine Geom=Check \n' % self.thr2
            myout.write('--Link1--\n')
            myout.write(''.join(parms))
            myout.write('\nMYMOL\n\n0 1\n\n')

        myout.close()
        sub.call("sed -i -- 's/ -1 /   /g' %s" % self.out ,shell=True )
        
    def ext_zmat(self,Log='cmove.log'):
        ''' Once a Gaussian log with Normal termination is given, a new matrix is written
            that contains the optimized internal degrees of freedom'''

        inp = open(Log).readlines()
        i=1 ; j=0 ; k=0
        
        # First take line number of Opt params line
        for line in inp:
            if '!   Optimized Parameters   !' in line:
                j = i+4 ; i = j
                break
            i += 1
        # Take the last line number after Opt params
        for line in inp[i:]:
            if '-----------------------------------------------------------------------' in line:
                k = i
                break
            i += 1

        #print('Lines extracted from Gauss log file are %d & %d' % (j,k) )
        out=open('tmp.log','w')
        out.write(''.join( inp[j:k] ) )
        out.close()

        head = ['Descrp' , 'Value']
        pmat  = pd.read_table('tmp.log',delim_whitespace=True, comment='#',names=head,usecols=(2,3))

        for rexp in pmat['Descrp']:

            Im = pmat[ (pmat.T == rexp).any() ].index[0]

            if len(self.zmat[ self.zmat.Batoms == rexp ].index) > 0:
                ind =  self.zmat[ self.zmat.Batoms == rexp ].index[0]
                #print rexp,self.zmat.loc[ind,'Dist'], mat.loc[Im,'Value']
                self.zmat.loc[ind,'Dist'] =  pmat.loc[Im,'Value']

            if len(self.zmat[ self.zmat.Aatoms == rexp ].index) > 0:
                ind =  self.zmat[ self.zmat.Aatoms == rexp ].index[0]
                #print rexp,self.zmat.loc[ind,'Angle'], mat.loc[Im,'Value']
                self.zmat.loc[ind,'Angle'] = pmat.loc[Im,'Value']

            if len(self.zmat[ self.zmat.Datoms == rexp ].index) > 0:
                ind =  self.zmat[ self.zmat.Datoms == rexp ].index[0]
                #print rexp,self.zmat.loc[ind,'Dihedral'], mat.loc[Im,'Value']
                self.zmat.loc[ind,'Dihedral'] =  pmat.loc[Im,'Value']  
                     
    def write_zmat(self,NAME):
        # Write the new matrix   
        self.out = NAME
        myout = open( self.out , 'w')
        #self.zmat.replace(-1,np.NaN,inplace=True)
        myout.write( self.zmat.to_string(header=False,index=False,na_rep='',columns=self.heads,float_format='%13.10f') )
        myout.close()
        sub.call("sed -i -- 's/ -1 /   /g' %s" % self.out ,shell=True )
         
    def ext_cart(self,Log='cmove.log'):
        ''' Once a Gaussian log with Normal termination is given, a new matrix is written
            that contains the optimized internal degrees of freedom'''

        inp = open(Log).readlines()
        j = []
        
        # First take line number of Opt params line
        for i,line in enumerate(inp):
            if 'Coordinates (Angstroms)' in line:
                j.append(i+3)
                        
        j = j[-1]

        #print( ''.join(inp[j:j+self.N]) )
        out=open('tmp.log','w')
        out.write( ''.join(inp[j:j+self.N]) ) 
        out.close()
        
        self.cart  = pd.read_table('tmp.log',delim_whitespace=True, comment='#',names=self.chead,usecols=(1,3,4,5))
        self.cart['Element'] = self.zmat['Element'].values 
        
    def write_cart(self,NAME):
        # Write the new matrix   
        self.out = NAME
        myout = open( self.out , 'w')
        myout.write( self.cart.to_string(header=False,index=False,na_rep='',columns=self.chead,float_format='%9.5f') )
        myout.close()
        
    def write_rmat(self,name):
        # Write the new matrix with reduced number of decimals for zmat 
        self.out = name
        myout = open( self.out , 'w')
        myout.write( self.zmat.to_string(header=False,index=False,\
                    columns=self.heads,formatters=self.forms,na_rep='') )
        myout.close()
        sub.call("sed -i -- 's/ -1 /   /g' %s" % self.out ,shell=True )
        sub.call("sed -i -- 's/nan/   /g' %s"  % self.out ,shell=True )
